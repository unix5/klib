#include "stdio.h"
#include "../drivers/tty.h"

int kprint_set = -1;
void (*print)(int, int, char*);

void kprint(char *string)
{
	if(!kprint_set) { // TODO: Change the print command based on the video mode
		print = &tty_print;
	}

	for (int i = 0; string[i] != '\0'; ++i)
		print(4, i, string[i]);
}