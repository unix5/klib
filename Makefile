TARGET_LIB = libk.so

CC 		= gcc
CFLAGS 	= -O3 -fPIC
DEBUG_CFLAGS = -Wall -Werror -pedantic -ggdb3 -Wno-error=unknown-pragmas -fPIC

SRC_DIRS 	= ./
BUILD_DIR 	= ./build

SRCS = $(shell ls $(SRC_DIRS)/*.c)
HEADERS = $(shell ls $(SRC_DIRS)/*.h)
OBJS = $(SRCS:%=$(BUILD_DIR)/%.o)

all: $(BUILD_DIR)/$(TARGET_LIB)

# Program binary
$(BUILD_DIR)/$(TARGET_LIB): $(OBJS)
	$(CC) --shared $(OBJS) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)

ifdef DEBUG
	$(CC) $(DEBUG_CFLAGS) -c $< -o $@
else
	$(CC) $(CFLAGS) -c $< -o $@
endif

clean:
	$(RM) -rf $(BUILD_DIR)
